import java.util.Arrays;


public class ColorSort {

   enum Color {red, green, blue};
   
   public static void main (Color[] balls) {
	   int red=0;
       int blue=balls.length-1;
       
       for(int i=0; i<blue; i++){
           if(red < blue){
               if(balls[i] == Color.red){
            	   Color tmp = balls[i];
            	   balls[i] = balls[red];
            	   balls[red] = tmp;
                   red++;
               }
               else if(balls[i] == Color.blue){
            	   Color tmp = balls[i];
                   balls[i] = balls[blue];
                   balls[blue] = tmp;
                   blue--;
                   i--;
               }
           }
       }
       System.out.println(Arrays.toString(balls));
	}

 
public static void reorder (Color[] balls) {
	 int lo = 0, mid = 0, hi = balls.length - 1;
	 
     while (mid <= hi)
         switch (balls[mid]) {
             case red:
                 swap(balls, lo++, mid++);
                 break;
             case green:
                 mid++;
                 break;
             case blue:
                 swap(balls, mid, hi--);
                 break;
         }
}


private static void swap(Color[] balls, int i, int j) {
        Color tmp = balls[i];
        balls[i] = balls[j];
        balls[j] = tmp;
}
	/*int red=0;
    int blue=balls.length-1;
    
    for(int i=0; i<blue; i++){
        if(red < blue){
            if(balls[i] == Color.red){
         	   Color tmp = balls[i];
         	   balls[i] = balls[red];
         	   balls[red] = tmp;
                red++;
            }
            else if(balls[i] == Color.blue){
         	   Color tmp = balls[i];
                balls[i] = balls[blue];
                balls[blue] = tmp;
                blue--;
                i--;
            }
        }
    }
  
   } */

   
}
